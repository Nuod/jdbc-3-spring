package my.db.sql.dao;

import my.db.sql.dto.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class BidDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Bid> RowMap = (rowStr, rowNum) -> new Bid(rowStr.getInt("bid_id"),
            rowStr.getDate("bid_date").toLocalDate(), rowStr.getInt("bid_value"),
            rowStr.getInt("items_item_id"),
            rowStr.getInt("users_user_id"));

    public List<Bid> getAllBidsOfUser(int user) {
        String sql = "SELECT * from bids WHERE users_user_id =?";

        return jdbcTemplate.query(sql, new Object[]{user}, RowMap);
    }

    public List<Map<String, Object>> getMaxBidByItems() {
        String sql = "SELECT title, MAX(bid_value) " +
                "FROM items INNER JOIN bids ON items.item_id = bids.items_item_id " +
                "GROUP BY title";
        return jdbcTemplate.queryForList(sql);
    }

    public void DeletUsersBids(int user) {
        String sql = "delete from bids where bids.users_user_id = ?";
        jdbcTemplate.update(sql, new Object[]{user});
    }
    public List<Bid> ShowBids() {
        String sql = "select * from bids";
        return jdbcTemplate.query(sql, RowMap);
    }
}
