package my.db.sql.dao;

import com.sun.rowset.internal.Row;
import my.db.sql.dto.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ItemDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Item> RowMap = (rowStr, rowNum) -> new Item(
            rowStr.getInt("item_id"),
            rowStr.getString("title"),
            rowStr.getString("descriptions"),
            rowStr.getDouble("start_price"),
            rowStr.getDouble("bid_increment"),
            rowStr.getDate("start_date").toLocalDate(),
            rowStr.getDate("stop_date").toLocalDate(),
            rowStr.getInt("by_it_now"),
            rowStr.getInt("users_user_id"));

    public List<Item> ItemsOfUser(int user) {
        String sql = "select * from items where users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{user}, RowMap);
    }

    public List<Item> SearchItemDescrip(String search) {
        String sql = "select * from items where descriptions like ?";
        search = "%" + search + "%";
        return jdbcTemplate.query(sql, new Object[]{search}, RowMap);
    }

    public List<Item> SearchItemTitle(String search) {
        String sql = "select * from items where  title like ?";
        search = "%" + search + "%";
        return jdbcTemplate.query(sql, new Object[]{search}, RowMap);
    }

    public List<Map<String, Object>> getAveragePriceByUsers() {
        String sql = "SELECT full_name, AVG(start_price) " +
                "FROM items INNER JOIN users ON items.users_user_id = users.user_id " +
                "GROUP BY users_user_id";
        return jdbcTemplate.queryForList(sql);
    }

    public List<Item> ActivItem(int user) {
        String sql = "select users.full_name, items.title from users, items where " +
                "users.user_id = items.users_user_id and items.by_it_now = 1 and items.users_user_id = ?";
        return jdbcTemplate.query(sql, new Object[]{user}, RowMap);
    }

    public void AddItem(Item item) {
        String sql = "INSERT INTO items (`item_id`, `title`, `descriptions`, `start_price`, `bid_increment`, `start_date`, `stop_date`, `by_it_now`, `users_user_id`)" +
                " VALUES (?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, new Object[]{item.getId(),
                item.getTitle(),
                item.getDescription(),
                item.getStart_price(),
                item.getBid_increment(),
                Date.valueOf(item.getStart_date()),
                Date.valueOf(item.getEnd_date()),
                item.getBy_it_now(),
                item.getUser_id()
        });

    }

    public void DeletUsersItems(int user) {
        String sql = "delete items from items where users_user_id = ?";
        jdbcTemplate.update(sql, new Object[]{user});
    }

    public void MultPrice(int user) {
        String sql = "update items set items.start_price = items.start_price*2.0 where items.users_user_id = ?";
        jdbcTemplate.update(sql, new Object[]{user});
    }

    public List<Item> ShowItems() {
        String sql = "select * from items";
        return jdbcTemplate.query(sql, RowMap);
    }
}
