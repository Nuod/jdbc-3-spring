package my.db.sql.dao;

import my.db.sql.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<User> RowMap = (rowStr, rowNum) -> new User(
            rowStr.getInt("user_id"),
            rowStr.getString("full_name"),
            rowStr.getString("billing_address"),
            rowStr.getString("login"),
            rowStr.getString("password"));

    public void AddUser(User user) {
        String sql = "INSERT INTO users (`user_id`, `full_name`, `billing_address`, `login`, `password`)" +
                " VALUES (?,?,?,?,?);";
        jdbcTemplate.update(sql, new Object[]{user.getId(),
                user.getFull_name(),
                user.getBilling_address(),
                user.getLogin(),
                user.getPassword()});
    }
    public List<User> ShowUsers() {
        String sql = "select * from users";
        return jdbcTemplate.query(sql, RowMap);
    }
}
