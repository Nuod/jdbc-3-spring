package my.db.sql.dto;

import java.time.LocalDate;

public class Item {
    private int id;
    private String title;
    private String description;
    private double start_price;
    private double bid_increment;
    private LocalDate start_date;
    private LocalDate end_date;
    private int by_it_now;
    private int user_id;

    public Item(int id,String title, String description, double start_price, double bid_increment,
                LocalDate start_date, LocalDate end_date,int by_it_now, int user_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.end_date = end_date;
        this.by_it_now = by_it_now;
        this.user_id = user_id;
    }

    public Item(int id, String title, String description, double start_price, double bid_increment,
                LocalDate start_date, LocalDate end_date, int user_id) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.end_date = end_date;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStart_price() {
        return start_price;
    }

    public void setStart_price(double start_price) {
        this.start_price = start_price;
    }

    public double getBid_increment() {
        return bid_increment;
    }

    public void setBid_increment(double bid_increment) {
        this.bid_increment = bid_increment;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getEnd_date() {
        return end_date;
    }

    public void setEnd_date(LocalDate end_date) {
        this.end_date = end_date;
    }

    public int getBy_it_now() {
        return by_it_now;
    }

    public void setBy_it_now(int by_it_now) {
        this.by_it_now = by_it_now;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return String.format(
                "id= %d;\n title= %s;\n description= %s;\n start_price= %f;\n bid_increment= %f;\n " +
                        "start_date= %s;\n end_date= %s;\n user_id= %s\n",
                id, title, description, start_price, bid_increment, start_date, end_date, user_id);
    }
}
