package my.db.sql.dto;

import java.time.LocalDate;

public class Bid {

    private int id;
    private LocalDate bid_date;
    private int bid_value;
    private int item_id;
    private int user_id;

    public Bid(int id, LocalDate bid_date, int bid_value, int item_id, int user_id) {
        this.id = id;
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.item_id = item_id;
        this.user_id = user_id;
    }

    /*public Bid(LocalDate bid_date, int bid_value, int item_id, int user_id) {
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.item_id = item_id;
        this.user_id = user_id;
    }*/

    @Override
    public String toString() {
        return String.format("id= %d;\n bid_date= %s;\n bid_value= %s;\n item_id= %s;\n user_id= %s\n", id, bid_date, bid_value, item_id, user_id);
    }
}

