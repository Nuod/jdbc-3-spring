package my.db.sql.demoStart;

import my.db.sql.dao.BidDao;
import my.db.sql.dao.ItemDao;
import my.db.sql.dao.UserDao;
import my.db.sql.dto.Item;
import my.db.sql.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

//6, 'Печкин', 'Владивосток', 'Pecha', '31351fdf'
//6, "cat", "animal", 10, 5, "2019-01-10", "2019-02-01", 1, 5


@Service
public class Demo implements IDemo {
    @Autowired
    BidDao bidDao;
    @Autowired
    ItemDao itemDao;
    @Autowired
    UserDao userDao;

    @Override
    public void Start() {
        System.out.println("1 Список ставок данного пользователя 1");
        bidDao.getAllBidsOfUser(1).forEach(System.out::println);

        System.out.println("2 Список лотов данного пользователя 1");
        itemDao.ItemsOfUser(1).forEach(System.out::println);

        System.out.println("3 Поиск лотов по подстроке в названии");
        itemDao.SearchItemTitle("o").forEach(System.out::println);

        System.out.println("4 Поиск лотов по подстроке в описании");
        itemDao.SearchItemDescrip("o").forEach(System.out::println);

        System.out.println("5 Средняя цена лотов каждого пользователя");
        itemDao.getAveragePriceByUsers().forEach(System.out::println);

        System.out.println("6 Максимальный размер имеющихся ставок на каждый лот");
        bidDao.getMaxBidByItems().forEach(System.out::println);

        System.out.println("7 Список действующих лотов данного пользователя");
        itemDao.ActivItem(1).forEach(System.out::println);

        System.out.println("8 Добавить нового пользователя");
        userDao.AddUser(new User(6, "Печкин", "Владивосток", "Pecha", "31351fdf"));

        System.out.println("9 Добавить новый лот");
        itemDao.AddItem(new Item(6,
                "cat",
                "animal",
                10.0,
                5.0,
                LocalDate.of(2019,01,10),
                LocalDate.of(2019,02,01),
                1,
                5));
        System.out.println("10 Удалить ставки данного пользователя");
        bidDao.DeletUsersBids(1);

        System.out.println("11 Удалить лоты данного пользователя");
        itemDao.DeletUsersItems(1);

        System.out.println("12 Увеличить стартовые цены товаров данного пользователя вдвое");
        itemDao.MultPrice(5);

    }
}
