package demo;


import my.db.sql.AppConfig;
import my.db.sql.demoStart.IDemo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		//SpringApplication.run(DemoApplication.class, args);

		new AnnotationConfigApplicationContext(AppConfig.class).getBean(IDemo.class).Start();
	}
}

